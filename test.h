#ifndef NUT_H
#define NUT_H

#include <SDL2/SDL.h>

typedef unsigned long size_t;

typedef struct Fruit {
	struct Fruit* next;
	struct Fruit* prev;
	int sweetness;
} Fruit;

typedef struct {
	const char* family;
	const char* name;
	size_t id;
	Fruit* fruits;
} Tree;

#endif
