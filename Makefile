exe = analysis.exe

.PHONY: run run_fast

build: 
	gcc -ggdb main.cc -lclang -o $(exe)

run: build
	./$(exe) s

run_fast: build pch
	./$(exe) f

pch: 
	clang -x c test.h -emit-ast -o test.pch

