# llvm-analysis-test
A small project for testing clang-c to get types available inside of a header file. This is usefull for supporting a cffi in other languages.

## Usage
```bash
make build 	        # Build project
make pch 		    # Build test pch for fast mode
make run 	        # Always slow mode
./analysis.exe f 	# Use fast mode(pch)
```

## Dependencies
### Arch Linux (btw.)
```bash
sudo pacman -S llvm git make gcc 
```
