#include <clang-c/Index.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum CXChildVisitResult TranslationUnitVisitor(CXCursor cursor, CXCursor parent, CXClientData client_data)
{
    // Return one of CXChildVisitResult

    if (cursor.kind == CXCursor_TypedefDecl)
    {
        printf("CXCursor_TypedefDecl=%d\n", cursor.kind);

        CXType t = clang_getTypedefDeclUnderlyingType(cursor);

        CXString s = clang_getTypeSpelling(t);
        printf("\t%s\n", clang_getCString(s));

        size_t size = clang_Type_getSizeOf(t);
        printf("\tsizeof=%d\n", size);

        printf("\n");
    }

    return CXChildVisit_Continue;
}

int main(int argc, const char **argv)
{
    CXIndex idx = clang_createIndex(1, 1);

    if (argc < 2)
    {
        printf("USE: %s <mode (f|s)>\n", argv[0]);
        return 1;
    }

    char *args[] = {0};
    CXTranslationUnit tu;

    if (strcmp("f", argv[1]) == 0)
    {
        tu = clang_createTranslationUnit(idx, "test.pch");
    }
    else if (strcmp("s", argv[1])==0)
    {
        tu = clang_createTranslationUnitFromSourceFile(idx, "test.h", 0, args, 0, 0);
    }
    else
    {
        printf("USE: %s <mode (f|s)>\n", argv[0]);
        return 1;
    }

    clang_visitChildren(clang_getTranslationUnitCursor(tu), TranslationUnitVisitor, 0);

    clang_disposeTranslationUnit(tu);
}